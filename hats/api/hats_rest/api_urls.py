from django.urls import path
from .views import hats_list, api_show_hat

urlpatterns = [
    path("hats/", hats_list, name="hats_list"),
    path("hats/<int:pk>/", api_show_hat, name="hats_show"),
]
