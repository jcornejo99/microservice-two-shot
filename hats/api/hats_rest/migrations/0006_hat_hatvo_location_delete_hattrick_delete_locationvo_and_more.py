# Generated by Django 4.0.3 on 2023-07-24 13:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0005_alter_hattrick_location'),
    ]

    operations = [
        migrations.CreateModel(
            name='Hat',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fabric', models.CharField(max_length=100)),
                ('style_name', models.CharField(max_length=100)),
                ('color', models.CharField(max_length=100)),
                ('hat_url', models.URLField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='HatVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('closet_name', models.CharField(max_length=100)),
                ('import_href', models.CharField(max_length=100, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('closet_name', models.CharField(max_length=100)),
                ('section_number', models.PositiveSmallIntegerField()),
                ('shelf_number', models.PositiveSmallIntegerField()),
            ],
            options={
                'ordering': ('closet_name', 'section_number', 'shelf_number'),
            },
        ),
        migrations.DeleteModel(
            name='HatTrick',
        ),
        migrations.DeleteModel(
            name='LocationVO',
        ),
        migrations.AddField(
            model_name='hat',
            name='location',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='location', to='hats_rest.hatvo'),
        ),
    ]
