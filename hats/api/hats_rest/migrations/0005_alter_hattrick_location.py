# Generated by Django 4.0.3 on 2023-07-20 20:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0004_locationvo_remove_hattrick_test_hattrick_location'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hattrick',
            name='location',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='hats_rest.locationvo'),
        ),
    ]
