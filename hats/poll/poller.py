import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import HatVO

# import models from hats_rest, here.
# from shoes_rest.models import Something


def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            response = requests.get('http://wardrobe-api:8000/api/locations/')
            content = json.loads(response.content)

            for location in content["locations"]:
                HatVO.objects.update_or_create(
                    import_href=location["href"],
                    defaults={"closet_name": location["closet_name"]},
                )
                print("HatVO was created")
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
