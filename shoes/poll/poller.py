import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

from shoes_rest.models import BinVO
# Import models from hats_rest, here.
# from shoes_rest.models import Something


def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            # Write your polling logic, here
            response = requests.get('http://wardrobe-api:8000/api/bins')
            content = json.loads(response.content)
            print(content)
            for bin in content["bins"]:
                BinVO.objects.update_or_create(
                    import_href=bin["href"],
                    #the Bin object is passing down the closet_name property as the value
                    #to an instance of the BinVO object
                    #The poller is polling wardrobe API to create an instance of the BinVO
                    #to verify run in app docker terminal running the shoes app and enter 'from shoes_rest.models import BinVO'
                    # THEN, 'BinVO.objects.all()' to check that a BinVO was created
                    defaults={"name": bin["closet_name"]},
                    # "name" here comes from the attribute in BinVO model
                    # "closet_name comes from the attribute in Bin under wardrobe_api"
                )

        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
