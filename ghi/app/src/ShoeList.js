function ShoeList({  shoes, setShoes }) {
    if (shoes === undefined) {
        return null;
    }


    const handleDelete = async (shoeId) => {
        const shoeUrl = `http://localhost:8080/api/shoes/${shoeId}`;
        const response = await fetch(shoeUrl, {method: "DELETE"});
        if (response.ok) {
            const newList = shoes.filter(shoe => shoe.id !== shoeId)
            setShoes(newList);
        }
        else {
            console.error("An error occurred deleting the item, please try again.")
        }
    }

    return (
        <>
        <div className="container">
            <table className="table table-success table-striped table-responsive table-hover">
                <thead className="table-primary">
                    <tr>
                        <th>Model Name</th>
                        <th>Manufacturer</th>
                        <th>Color</th>
                        <th>Bin</th>
                        <th>Picture URL</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map((shoe, index) => {
                        return (
                                <tr key={index}>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.bin.name}</td>
                                {/* <td ><img className="img-fluid ratio ratio-16x9" src={shoe.picture_url}/></td> */}
                                <td >src={shoe.picture_url}</td>
                                <td><button type="button"  className="btn btn-danger btn-md active" onClick={() => handleDelete(shoe.id)}>Delete</button></td>
                                <td><button className="btn btn-primary btn-md active">Edit</button></td>
                                </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
        </>
    )
}
export default ShoeList
