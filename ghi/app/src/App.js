import React, { useEffect, useState } from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeForm from './ShoeForm'
import ShoeList from './ShoeList'
import ListHats from './ListHats';
import CreateHat from './CreateaHat';




function App(props) {
  const [shoes, setShoes] = useState([]);
  console.log(shoes);
  const [hats, setHats] = useState([]);

  async function loadHats() {
    const response = await fetch("http://localhost:8090/api/hats/");
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    } else {
      console.error(response);
    }
  }

  const fetchShoes = async () => {
    const shoesUrl = 'http://localhost:8080/api/shoes/'
    const response = await fetch(shoesUrl);
    if (response.ok) {
      // the shoes in const { shoes } comes from the shoes state variable
      const { shoes } = await response.json()
      setShoes(shoes)
    }
    else {
      console.error('Could not fetch the  data from the database')
    }
  }


  useEffect(() => {
    fetchShoes();
    loadHats();
  }, [])

  if (shoes === undefined) {
    return null;
  }


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats">
            <Route index element={<ListHats hats={hats} loadHats={loadHats}/>} />
            <Route path="new" element={<CreateHat loadHats={loadHats}/>} />
          </Route>
          <Route path="shoes">
            {/* by passing the shoe state variable to the ShoeList component we're able to do a console console.log
            statement right after the ShoeList function declaration to figure out what to map by looking at the
            structure of the shoes object in the dev tools */}
              <Route index element={<ShoeList shoes={shoes} setShoes={setShoes} />} />
            {/* fetchShoes fn is passed down to ShoeForm fn (prop drilling) specifically */}
            {/* inside the handleSubmit so that after the shoe is created in the form by user
            and form clears the input fields the newly created shoe item gets added to
            shoe list page without having to manually refresh */}
              <Route path="new" element={<ShoeForm fetchShoes={fetchShoes} /> } />
          </Route>
        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;
