import React, { useEffect, useState } from 'react'


function ShoeForm({fetchShoes}) {
    const [formKey, setFormKey] = useState(0);
    const [bins, setBins] = useState([]);
    const [modelName, setModelName] = useState('')
    const[color, setColor] = useState('')
    const[manufacturer, setManufacturer] = useState('')
    const[bin, setBin] = useState('')
    const[pictureUrl, setPictureUrl] = useState('')

    const fetchBins = async () => {
        const binsUrl = 'http://localhost:8100/api/bins/'
        const response = await fetch(binsUrl)
        if (response.ok) {
          const data = await response.json()
          console.log(data)
          setBins(data.bins)
        }
      }
      useEffect(() => {
        fetchBins();
      }, [])
    const handleModelNameChange = event => {
        const value = event.target.value;
        setModelName(value);
    }
    const handleColorChange = event => {
        const value = event.target.value;
        setColor(value);
    }
    const handleManufacturerChange = event => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const handlePicturUrlChange = event => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleBinChange = event => {
        const value = event.target.value;
        setBin(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            //the keys correspond to keys in dictionary in insomnia
            manufacturer: manufacturer,
            model_name: modelName,
            color: color,
            picture_url: pictureUrl,
            bin: bin
        }
        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        }

    };
        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            setModelName('');
            setManufacturer('');
            setColor('');
            setPictureUrl('');
            setBin('');

            setFormKey((priorKey) => priorKey  + 1)

            fetchShoes();
        }


}

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Shoe</h1>
                <form key={formKey} onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                    <input onChange={handleModelNameChange}  placeholder="model name" required type="text" name="model_name" id="model_name" className="form-control"/>
                    <label htmlFor="model_name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange}  placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePicturUrlChange}  placeholder="picture URL" required type="text" name="pictureUrl" id="pictureUrl" className="form-control"/>
                    <label htmlFor="color">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                        <option value="">Choose a bin</option>
                        {bins.map( bin => {
                            return <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    )
}

export default ShoeForm
