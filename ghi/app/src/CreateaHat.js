import React, { useEffect, useState } from 'react'

function HatForm({fetchHats}) {
    const [formKey, setFormKey] = useState(0);
    const [locations, setLocations] = useState([]);
    const [modelName, setModelName] = useState('');
    const [styleName, setStyleName] = useState(''); // Add this for style_name
    const [color, setColor] = useState('');
    const [fabric, setFabric] = useState('');
    const [location, setLocation] = useState(null);
    const [hat_url, setHat_Url] = useState('');

    const fetchLocations = async () => {
        const locationsUrl = 'http://localhost:8080/api/locations/'
        const response = await fetch(locationsUrl)
        if (response.ok) {
            const data = await response.json()
            console.log(data)
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchLocations();
    }, [])

    const handleModelNameChange = event => setModelName(event.target.value);
    const handleStyleNameChange = event => setStyleName(event.target.value); // Add this for style_name
    const handleColorChange = event => setColor(event.target.value);
    const handleFabricChange = event => setFabric(event.target.value);

    const handleLocationChange = event => {
        const selectedLocationHref = event.target.value;
        const selectedLocation = locations.find(loc => loc.import_href === selectedLocationHref);
        setLocation(selectedLocation);
    };

    const handleHatUrlChange = event => setHat_Url(event.target.value);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            model_name: modelName,
            style_name: styleName, // Add this for style_name
            color: color,
            fabric: fabric,
            location: location,
            hat_url: hat_url,
        }
        const hatsUrl = 'http://localhost:8080/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setModelName('');
            setStyleName(''); // Add this for style_name
            setColor('');
            setFabric('');
            setLocation(null);
            setHat_Url('');

            setFormKey((priorKey) => priorKey + 1)

            fetchHats();
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Hat</h1>
                <form key={formKey} onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={handleModelNameChange} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                    <label htmlFor="model_name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleStyleNameChange} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control"/> {/* Add this for style_name */}
                    <label htmlFor="style_name">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                    <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                        <option value="">Choose a Location</option>
                        {locations.map( location => {
                            return <option key={location.id} value={location.href}>{location.closet_name}</option>
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleHatUrlChange} placeholder="Hat URL" required type="text" name="hat_url" id="hat_url" className="form-control"/>
                    <label htmlFor="hat_url">Hat URL</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    )
}

export default HatForm
