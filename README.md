# Wardrobify

Team:

* Jonathan Cornejo - Shoes!!!
* Francisco Real - Hats!!

## Design
**Front-end:**

The React application provides the user interface and handles the client-side logic. It includes components for viewing, adding, and deleting shoes and hats. It communicates with the back-end through RESTful APIs.

**Back-end:**

The back-end consists of several microservices.

1. **Wardrobe API:** This is a RESTful API that provides data about locations and bins. It's used by the hats and shoes pollers to fetch data.

2. **Shoes API:** This RESTful API handles the shoes resources. It provides endpoints for fetching a list of shoes, adding a new shoe, and deleting a shoe.

3. **Shoes Poller:** This service polls the Wardrobe API for bin data, which is relevant for the shoes.

4. **Hats API:** Like the Shoes API, this RESTful API handles the hats resources, providing similar functionalities for hats.

5. **Hats Poller:** This service polls the Wardrobe API for location data, relevant for the hats.

6. **Database:** A PostgreSQL database holds all the data for these microservices.
## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

The shoes model is connected to the wardrobe microservice since we are polling from wardrobe API
in order to poll data about the Bin model and pass down a unique property to an instance of BinVO
while updating or creating a BinVO.

## Hats microservice

Hats Microservice Model
The main model in the Hats microservice is Hat with attributes: model_name, style_name, color, fabric, hat_url, and location. Location references the Location model in the Wardrobe microservice, establishing a one-to-many relationship: a location can store many hats, but each hat has only one location.

Integration with Wardrobe Microservice
The Hats microservice integrates with the Wardrobe service through the location attribute. To create a Hat, a location from the Wardrobe service is needed. The Hats service fetches locations through an API call to /api/locations/ in the Wardrobe service, and these locations are then available for selection when creating a new Hat.

When a new Hat is created, the chosen location is included in the request, linking the Hat and its Location. This makes the Hats service dependent on the Wardrobe service for managing hat locations, ens
